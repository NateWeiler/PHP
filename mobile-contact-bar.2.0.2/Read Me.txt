=== Mobile Contact Bar ===
Tags: social media, icon, contact, mobile, woocommerce cart
Requires at least: 4.2
Tested up to: 5.2
Requires PHP: 5.3
Stable tag: 2.0.2

Allow your visitors to contact you via mobile phones, or access your site's pages instantly.


== Description ==

Mobile Contact Bar is a compact and highly customizable plugin, which allows your visitors to contact you directly via mobile phones,
or access your site's pages instantly.

The settings page is available under the *Settings &rarr; Mobile Contact Bar* menu in the WordPress dashboard.



= Features =

* Icons for social media, call-to-actions, or any links to web pages
* Simple and intuitive styling with the aid of the Real-time Model
* Built-in icon picker with [Font Awesome 5](https://fontawesome.com/) integration
* Customizable URLs using query string parameters
* No data collection from your website's visitors
* Super easy to use, no coding required!



= Special Icons =

* Scroll to Top
* WooCommerce Cart with Item Counter



= Supported Protocols =

* `http`
* `https`
* `mailto`
* `skype`
* `sms`
* `tel`


= Tested with =

* Twenty Nineteen
* Twenty Seventeen
* Twenty Sixteen
* Twenty Fifteen
* Twenty Fourteen
* Twenty Thirteen
* Twenty Twelve
* Twenty Eleven
* Twenty Ten

== Installation ==

= First time Mobile Contact Bar user =

Thank you for choosing Mobile Contact Bar! In order to create your bar, simply activate the plugin and visit the plugin's settings page by clicking on *Settings &rarr; Mobile Contact Bar* in your left navigation menu.
Once the plugin page loads, open the *Bar* box, choose the *Display on Devices* option, select the device type to enable the bar and then press the *Save Changes* button at the bottom of the page.
Mobile Contact Bar will automatically create a default bar with an envelope icon, which uses the email address of your site's admin.

= Adding icons to your bar =

To add more icons to your bar, open the *Contact List* box, find a particular list item, select the checkbox, customize the icon and fill in the URI field.
In order to add custom links, click on the *New Contact* button or on one of the icons at the top of the list.


= Positioning and styling your bar =

To set options for bar (positions, colors, borders, width, height, space, placeholder, etc.), open the *Bar* box and check the changes on the *Real-time Model*.
Open the *Icons*, *Badges*, or *Toggle* box and set options for icons, badges, or toggle, respectively.


== Frequently Asked Questions ==

= JavaScript disabled =
The plugin works fine without JavaScript on the front-end of your site.
We use JavaScript on the front-end in two cases:
1. if the toggle is activated, the plugin has an option for saving the toggle state in a cookie, and
2. if the *Scroll to Top* icon is added, it calculates scrolling position in an inline script.

= Cookies =
You have full control over your single cookie.



== Screenshots ==

1. Settings &rarr; Mobile Contact Bar
2. Bar box
3. Icons box, Toggle box
4. Contact List box